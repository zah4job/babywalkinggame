using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class enter : MonoBehaviour
{
    public UnityEvent onEnter;
    public UnityEvent onDie;
    public PlayerMovement player;
    public string targetItem = "unknown";

    private void OnTriggerEnter2D(Collider2D collision)
    {
        Debug.Log("enter the cave");
        Debug.Log("player have " + player.item);
        if (player.item != targetItem)
        {
            onDie.Invoke();
        }
        player.takeItem("", null);
    }

    private void OnMouseDown()
    {
        Debug.Log("mouse down");
    }
}
