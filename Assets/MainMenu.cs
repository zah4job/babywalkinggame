using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEditor;

public class MainMenu : MonoBehaviour
{
    public void LoadLevel(string levelName)
    {
        if (levelName == "coming_soon")
        {
            //EditorUtility.DisplayDialog("Coming soon", "New levels will coming soon", "OK", "YES");
            return;
        }
        SceneManager.LoadScene(levelName);
    }
}
