using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class PlayerMovement : MonoBehaviour
{
    public string item = "";
    public Rigidbody2D rb;
    public GameObject inventory;
    public GameObject pickedItem;
    [SerializeField] float hSpeed = 5.5f;
    //[SerializeField] float vSpeed = 7f;

    Animator animator;
    SpriteRenderer sprite;
    bool _enableMovement = true;
    gameCycle gameCycleObject;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        animator = GetComponent<Animator>();
        sprite = GetComponent<SpriteRenderer>();

        //set inner equipment sprite to None
        takeItem("", null);
        //gameCycle for game flow controlling
        GameObject gameController = GameObject.FindGameObjectWithTag("GameController");
        gameCycleObject = gameController.GetComponent<gameCycle>();
    }

    // Update is called once per frame
    void Update()
    {
        rb.velocity = new Vector2(dirX() * hSpeed, rb.velocity.y);

        //if (Input.GetButtonDown("Jump"))
        //{
        //    rb.velocity = new Vector2(rb.velocity.x, vSpeed);
        //}

        bool isRunning = Mathf.Abs(rb.velocity.x) > 0;
        bool lookingLeft = rb.velocity.x < 0;
        animator.SetBool("running", isRunning);
        sprite.flipX = lookingLeft;
    }

    public bool haveItem()
    {
        return item != "";
    }

    public void takeItem(string itemName, Sprite itemSprite)
    {
        item = itemName;
        var eqObject = GameObject.FindGameObjectWithTag("Equipment");
        var sr = eqObject.GetComponent<SpriteRenderer>();
        sr.sprite = itemSprite;
    }

    public void pickItem(GameObject newItem)
    {
        var animationSprite = transform.Find("PickedItem").GetComponent<SpriteRenderer>();
        var pickedSprite = newItem.GetComponent<SpriteRenderer>().sprite;
        animationSprite.sprite = pickedSprite;
        animator.SetTrigger("pick");
    }

    public void Die()
    {
        gameCycleObject.LoadDieScene();
    }

    public void Win()
    {
        gameCycleObject.LoadWinScene();
    }

    public void disableMovement()
    {
        _enableMovement = false;
        Debug.Log("disabled");
    }

    public void enableMovement()
    {
        _enableMovement = true;
        Debug.Log("enabled");
    }

    /// <summary>
    /// Touch screen position
    /// </summary>
    /// <returns>-1 for left, 0 for nothing, 1 for right</returns>
    private int touchDirection()
    {
        if (Input.touchCount == 0)
        {
            return 0;
        }
        int width = Screen.currentResolution.width;
        var touchX = Input.GetTouch(0).position.x;
        if (touchX < width / 2)
        {
            return -1;
        }
        return 1;

    }

    private float dirX()
    {
        if (_enableMovement == false)
        {
            return 0;
        }

        float dir = Input.GetAxis("Horizontal");
        if (dir == 0)
        {
            dir = touchDirection();
        }
        return dir;
    }
}
