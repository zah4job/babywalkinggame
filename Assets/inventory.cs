using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class inventory : MonoBehaviour
{
    public string Name = "item";
    public PlayerMovement player;
    public GameObject invObject;

    private void OnMouseDown()
    {
        Debug.Log(string.Format("Selected Item: {0}", Name));
        var sr = GetComponent<SpriteRenderer>();
        var itemSprite = sr.sprite;
        player.takeItem(Name, itemSprite);
        player.enableMovement();
        invObject.SetActive(false);
    }
}
