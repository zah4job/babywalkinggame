using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundController : MonoBehaviour
{
    private AudioSource audioSource;
    private bool musicIsOn = true;

    // Start is called before the first frame update
    void Start()
    {
        audioSource = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        if (PlayerPrefs.HasKey("musicIsOn"))
        {
            musicIsOn = bool.Parse(PlayerPrefs.GetString("musicIsOn"));
        }
        audioSource.volume = musicIsOn ? 1 : 0;
    }
}
