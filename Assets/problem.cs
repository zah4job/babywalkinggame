using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class problem : MonoBehaviour
{
    public Sprite activeSprite;
    public Sprite solvedSprite;
    public List<GameObject> objects2Hide;
    public PlayerMovement player;
    public string solution;
    public GameObject pickOnSolution;
    public GameObject dropOnSolution;

    SpriteRenderer sr;
    bool solved = false;

    // Start is called before the first frame update
    void Start()
    {
        sr = GetComponent<SpriteRenderer>();
    }

    void Solve()
    {
        sr.sprite = solvedSprite;
        solved = true;
        foreach (var gobj in objects2Hide) {
            gobj.SetActive(false);
        }

        if (pickOnSolution && dropOnSolution)
        {
            var targetPosition = dropOnSolution.GetComponent<Transform>().position;
            var pickedTrans = pickOnSolution.GetComponent<Transform>();
            pickedTrans.position = targetPosition;
            dropOnSolution.SetActive(false);

            //start pickup animation
            player.pickItem(pickOnSolution);
        }

    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (solved) { return; }
        if (solution == "win")
        {
            player.Win();
            return;
        }
        if (solution == player.item)
        {
            Solve();
            MoveChest();
            player.takeItem("", null);
        }
        else
        {
            player.Die();
        }
    }

    private void MoveChest()
    {
        var chest = GameObject.FindGameObjectWithTag("Chest");
        chest.transform.position += new Vector3(20, 0);
    }
}
