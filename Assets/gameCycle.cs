using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Events;

public class gameCycle : MonoBehaviour
{
    public string newGameSceneName;
    public string dieSceneName;
    public string winSceneName;
    public string nextLevelName;
    public string currentLevelName;
    public string mainMenuName = "MainMenu";

    public void StartNewGame()
    {
        Debug.Log("load scene");
        SceneManager.LoadScene(newGameSceneName);
    }

    public void LoadDieScene()
    {
        SceneManager.LoadScene(dieSceneName);
    }

    public void LoadWinScene()
    {
        SceneManager.LoadScene(winSceneName);
    }

    public void LoadNextLevel()
    {
        SceneManager.LoadScene(nextLevelName);
    }

    public void RestartLevel()
    {
        SceneManager.LoadScene(currentLevelName);
    }

    public void GoMainMenu()
    {
        SceneManager.LoadScene(mainMenuName);
    }
}
