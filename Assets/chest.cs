using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class chest : MonoBehaviour
{
    public GameObject inventoryComponent;
    public PlayerMovement player;

    private void OnCollisionEnter()
    {
        print(1);
    }

    public void XXX() {
        print("xxx");
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (player.haveItem() == false)
        {
            inventoryComponent.SetActive(true);
            AudioSource audio = GetComponent<AudioSource>();
            audio.Play();
            player.disableMovement();
        }
        
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        //var invPanel = GameObject.FindGameObjectWithTag("inventory");
        inventoryComponent.SetActive(false);
    }
}
