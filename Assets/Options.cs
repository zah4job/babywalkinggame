using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Options : MonoBehaviour
{
    public Sprite onSprite;
    public Sprite offSprite;
    private bool musicIsOn;
    private Image im;

    // Start is called before the first frame update
    void Start()
    {
        im = GetComponent<Image>();

        if (PlayerPrefs.HasKey("musicIsOn"))
        {
            musicIsOn = bool.Parse(PlayerPrefs.GetString("musicIsOn"));
        } else
        {
            musicIsOn = true;
        }
        updateIcon();
    }

    //private void OnMouseDown()
    //{
    //    Debug.Log("switch Music");
    //    musicIsOn = !musicIsOn;
    //    print(musicIsOn);
    //    updateIcon();
        
    //}

    public void toggleMusic()
    {
        Debug.Log("switch Music");
        musicIsOn = !musicIsOn;
        print(musicIsOn);
        updateIcon();
    }

    private void updateIcon()
    {
        if (musicIsOn)
        {
            PlayerPrefs.SetString("musicIsOn", "true");
            im.sprite = onSprite;
        }
        else
        {
            PlayerPrefs.SetString("musicIsOn", "false");
            im.sprite = offSprite;
        }
    }
}
