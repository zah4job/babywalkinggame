﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Cainos.PixelArtTopDown_Basic
{
    //let camera follow target
    public class CameraFollow : MonoBehaviour
    {
        public Component target;
        public float lerpSpeed = 1.0f;

        private Vector3 offset;

        private Vector3 targetPos;
        private Rigidbody2D rb;
        private Transform targetTransform;
        private Vector3 shift;

        private void Start()
        {
            if (target == null) return;
            targetTransform = target.GetComponent<Transform>();
            shift = new Vector3(0.0f, 0f, 0f);
            rb = target.GetComponent<Rigidbody2D>();
            offset = transform.position - targetTransform.position + shift;
        }

        private void Update()
        {
            if (target == null) return;

            targetPos = targetTransform.position + offset;
            var shiftedPos = rb.velocity.x >= 0 ? targetPos + shift : targetPos - shift;
            transform.position = Vector3.Lerp(transform.position, shiftedPos, lerpSpeed * Time.deltaTime);
        }

    }
}
